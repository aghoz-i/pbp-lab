import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  //hmmm

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(   //Theme
          colorScheme: ColorScheme.fromSwatch(
            primarySwatch: Colors.blue,
          ),
          textTheme: const TextTheme(bodyText2: TextStyle(color: Color(0xff0096c7))),
        ),
        home: Scaffold(     //positioning
            backgroundColor: Color(0xff8ce8ff),
            appBar: AppBar(
                title: const Text('Bizzvest Homepage'),
                centerTitle: true,
            ),
            body: const Padding(//positioning
                padding: EdgeInsets.all(12.0),
                child: Text(    //basic widget
                    'BizzVest, Solusi untuk Perkembangan Bisnis Anda di Masa Pandemi COVID-19',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        height: 1, 
                        fontSize: 36, 
                    ),//naiss
                ),
            ),
        ),
    );
  }
}
