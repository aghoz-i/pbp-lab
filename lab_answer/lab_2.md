1. Apakah perbedaan antara JSON dan XML?
 JSON merepresentasikan datanya melalui bentuk objek, sedangkan XML merepresentasikan datanya melalui penggunaan *tag structure*

2. Apakah perbedaan antara HTML dan XML?
 HTML berfungsi untuk menampilkan data pada suatu halaman web, sedangkan XML hanya berfungsi untuk menyimpan data(bukan untuk menampilkan data).
